const crypto = require('crypto');

function pass(url, username, master_hash) {
    str = `${url}\t${username}\t${master_hash}`;
    hash = crypto.createHash('sha256').update(str).digest('hex');
    return hash;
}

exports.handler = function(event, context, callback) {
    body = JSON.parse(event['body']);
    hash = pass(body.url, body.username, body.master_hash)
    
    callback(null, {
        statusCode: 200,
        body: hash
    });
}